#!/usr/bin/env python

import sys
import os
import requests
from datetime import date
from bs4 import BeautifulSoup


if len(sys.argv) > 1:
    day = int(sys.argv[1])
else:
    day = date.today().day

input_filename = f"input{day}.txt"
sample_filename = f"sample{day}.txt"
solution_filename = f"day{day}.py"

print(f"fetching for {day} December")

if not os.path.exists(solution_filename):
    print(f"creating:", solution_filename)
    with open(solution_filename, "w") as fh:
        fh.write(f"""#!/usr/bin/env python

# https://adventofcode.com/2022/day/{day}

input_filename = "input{day}.txt"
input_filename = "sample{day}.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

for line in data:
    print(line)
""")
    os.chmod(solution_filename, 0o755)

if not os.path.exists(input_filename):
    print(f"fetching input:", input_filename)
    response = requests.get(f"https://adventofcode.com/2022/day/{day}/input", cookies=dict(session=os.environ['AOC_SESSION']))
    response.raise_for_status()
    with open(input_filename, "wb") as fh:
        fh.write(response.content)

if not os.path.exists(sample_filename):
    print(f"fetching sample:", sample_filename)
    response = requests.get(f"https://adventofcode.com/2022/day/{day}")
    response.raise_for_status()
    soup = BeautifulSoup(response.text, features="html.parser")
    for c in soup.select('code'):
        if len(c.text.splitlines()) < 3:
            continue
        with open(sample_filename, "w") as fh:
            fh.write(c.text)
        break

print(f"\nYou might want this:\n\nwatchexec -c ./day{day}.py")
