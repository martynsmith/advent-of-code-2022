#!/usr/bin/env python
from __future__ import annotations
from typing import NamedTuple, Iterable

from rich import print
import re

# https://adventofcode.com/2022/day/15

input_filename = "input15.txt"


# input_filename = "sample15.txt"


class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x + p.x, self.y + p.y)

    def manhattan_distance(self, other: Vector) -> int:
        return abs(self.x - other.x) + abs(self.y - other.y)


with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

sensors = []
beacons = set()

for line in data:
    sx, sy, bx, by = map(int, re.findall(r'-?\d+', line))
    s = Vector(sx, sy)
    b = Vector(bx, by)
    sd = s.manhattan_distance(b)
    sensors.append((s, sd))
    beacons.add(b)


def part1(y):
    min_x = min(s.x - sd for s, sd in sensors)
    max_x = max(s.x + sd for s, sd in sensors)
    count = 0
    for x in range(min_x, max_x + 1):
        v = Vector(x, y)
        if v in beacons:
            continue

        for s, sd in sensors:
            if s.manhattan_distance(v) <= sd:
                count += 1
                break

    return count


def get_boundary(s: Vector, sd: int) -> Iterable[Vector]:
    for d in range(sd + 1):
        yield Vector(s.x + d, s.y - sd - 1 + d)

    for d in range(sd + 1):
        yield Vector(s.x + sd + 1 - d, s.y + d)

    for d in range(sd + 1):
        yield Vector(s.x + d, s.y + sd + 1 - d)

    for d in range(sd + 1):
        yield Vector(s.x - sd - 1 + d, s.y + d)


def part2(start: Vector, end: Vector):
    for s, sd in sensors:
        for p in get_boundary(s, sd):
            if start.x <= p.x <= end.x and start.y <= p.y <= end.y:
                for s2, sd2 in sensors:
                    if s2.manhattan_distance(p) <= sd2:
                        break
                else:
                    return p.x * 4_000_000 + p.y


print(part1(2_000_000))
print(part2(Vector(0, 0), Vector(4_000_000, 4_000_000)))
