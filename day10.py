#!/usr/bin/env python

# https://adventofcode.com/2022/day/10

input_filename = "input10.txt"
# input_filename = "sample10.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

cycle = 0
x = 1
next_count = 20
measures = []
all = []

for line in data:
    match line.split(' '):
        case 'addx', v:
            cycle += 2
            if cycle >= next_count:
                measures.append(x * next_count)
                next_count += 40
            while len(all) < cycle:
                all.append(x)
            x += int(v)
        case 'noop',:
            cycle += 1
            if cycle >= next_count:
                measures.append(x * next_count)
                next_count += 40
            while len(all) < cycle:
                all.append(x)
        case '_':
            raise NotImplemented(line)

    # print(line, ' ||| ', cycle, x)

print("part1:", sum(measures))

print(all)

for cycle in range(240):
    if cycle % 40 == 0:
        print()

    if (cycle % 40) - 1 <= all[cycle] <= (cycle % 40) + 1:
        print("\u2588\u2588", end="")
    else:
        print('  ', end="")

print()

for line in data:
    match line.split(' '):
        case 'addx', v:
            cycle += 2
            if cycle >= next_count:
                measures.append(x * next_count)
                next_count += 40
            x += int(v)
        case 'noop',:
            cycle += 1
            if cycle >= next_count:
                measures.append(x * next_count)
                next_count += 40
        case '_':
            raise NotImplemented(line)
