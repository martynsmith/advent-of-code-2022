#!/usr/bin/env python

from collections import defaultdict
from rich import print
import re
from copy import deepcopy

# https://adventofcode.com/2022/day/5

input_filename = "input5.txt"
# input_filename = "sample5.txt"

with open(input_filename) as fh:
    data = [l.rstrip() for l in fh.readlines()]

stacks = defaultdict(list)

for line in data:
    if not line or line[1] == '1':
        break
    for i in range(9):
        try:
            if line[i * 4 + 1] != ' ':
                stacks[i + 1].insert(0, line[i * 4 + 1])
        except IndexError:
            pass

p1 = deepcopy(dict(stacks))
p2 = deepcopy(dict(stacks))

for line in data:
    if not line.startswith('move'):
        continue

    count, source, target = list(map(int, re.findall(r'\d+', line)))

    holding = []

    for i in range(count):
        p1[target].append(p1[source].pop())
        holding.append(p2[source].pop())

    for i in range(count):
        p2[target].append(holding.pop())


def print_stacks(prefix, stacks):
    print(prefix, end="")
    for i in range(9):
        try:
            print(stacks[i + 1][-1], end="")
        except IndexError:
            pass

    print()


print_stacks("part1: ", p1)
print_stacks("part1: ", p2)
