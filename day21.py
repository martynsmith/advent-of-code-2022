#!/usr/bin/env python

from rich import print

# https://adventofcode.com/2022/day/21

input_filename = "input21.txt"
# input_filename = "sample21.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

monkeys = {}


def generate_n(n):
    return lambda: n


def generate_op(m1, op, m2):
    match op:
        case '+':
            return lambda: monkeys[m1]() + monkeys[m2]()
        case '-':
            return lambda: monkeys[m1]() - monkeys[m2]()
        case '*':
            return lambda: monkeys[m1]() * monkeys[m2]()
        case '/':
            return lambda: monkeys[m1]() / monkeys[m2]()
        case _:
            raise NotImplementedError()


for line in data:
    match line.split(' '):
        case monkey, n:
            monkeys[monkey[:-1]] = generate_n(int(n))
        case monkey, m1, op, m2:
            if monkey[:-1] == 'root':
                monkeys['__part2__'] = generate_op(m1, '-', m2)
            monkeys[monkey[:-1]] = generate_op(m1, op, m2)
        case _:
            raise NotImplementedError()

print("part1:", monkeys['root']())

low = 0
high = 1e14
while True:
    mid = (high - low) // 2 + low
    monkeys['humn'] = lambda: mid

    calc = monkeys['__part2__']()

    if calc == 0:
        break
    elif calc < 0:
        high = mid
    else:
        low = mid

print("part2:", mid)
