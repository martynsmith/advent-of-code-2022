#!/usr/bin/env python

from rich import print
from functools import cmp_to_key

# https://adventofcode.com/2022/day/13

input_filename = "input13.txt"
# input_filename = "sample13.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

pairs = []

lines = iter(data)
for line in lines:
    pairs.append((
        eval(line),
        eval(next(lines)),
    ))

    try:
        next(lines)
    except:
        pass

def c2(left, right, debug=False):
    if not left and right:
        return True
    if left and not right:
        return False
    if not left and not right:
        return None

    if debug:
        print(f"- Compare {left} vs {right}")

    l, r = left[0], right[0]

    if isinstance(l, int) and isinstance(r, int):
        if l < r:
            return True
        elif l > r:
            return False
        else:
            return c2(left[1:], right[1:], debug=debug)

    if isinstance(l, list) and isinstance(r, list):
        result = c2(l, r, debug=debug)
        if result is not None:
            return result
        else:
            return c2(left[1:], right[1:], debug=debug)

    if isinstance(l, int):
        return c2([[l]] + left[1:], right, debug=debug)

    if isinstance(r, int):
        return c2(left, [[r]] + right[1:], debug=debug)

    raise Exception("nope")


def compare(left, right, depth=0):
    # print("  " * depth + "- Compare", left, "vs", right)
    if left == [] and right:
        return True
    if left and right == []:
        return False

    if left == [] and right == []:
        return None

    i = 0
    while len(left) > i and len(right) > i:
        if isinstance(left[i], list) and isinstance(right[i], list):
            result = compare(left[i], right[i])
            if result is not None:
                return result
            else:
                i += 1
                continue
        elif isinstance(left[i], list):
            result = compare(left[i], [right[i]])
            if result is not None:
                return result
            else:
                i += 1
                continue
        elif isinstance(right[i], list):
            result = compare([left[i]], right[i])
            if result is not None:
                return result
            else:
                i += 1
                continue

        # print("  " * depth + "  - Compare", left[i], "vs", right[i])
        if left[i] < right[i]:
            return True
        elif left[i] > right[i]:
            return False
        i += 1

    if len(left) > len(right):
        return False
    if len(left) < len(right):
        return True
    # print("??", i, left, right)
    return None

i = 1
part1 = 0
for left, right in pairs:
    if compare(left, right):
        part1 += i
    i += 1

print("part1:", part1)

all_packets = [l for l, _ in pairs] + [r for _, r in pairs] + [[[2]], [[6]]]
def rcompare(left, right):
    if compare(left, right):
        return -1
    else:
        return 1

sorted_packets = sorted(all_packets, key=cmp_to_key(rcompare))
print("part2:", (sorted_packets.index([[2]]) + 1) * (sorted_packets.index([[6]]) + 1))
