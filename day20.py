#!/usr/bin/env python

from __future__ import annotations

from rich import print
from aoclib import timer

# https://adventofcode.com/2022/day/20

input_filename = "input20.txt"
# input_filename = "sample20.txt"


class LinkedList:
    def __init__(self):
        self.head: Node = None

    def __repr__(self):
        node = self.head

        nodes = []
        while node is not None and (not nodes or node != self.head):
            nodes.append(str(node.data))
            node = node.next
        return ", ".join(nodes)


class Node:
    def __init__(self, data):
        self.data = data
        self.prev = None
        self.next = None

    def __repr__(self):
        return f"<Node {self.data} {getattr(self.prev, 'data', '.')} {getattr(self.next, 'data', '.')}>"

llist = LinkedList()
to_process = []
zero = None

head = None
with open(input_filename) as fh:
    for n in fh.readlines():
        node = Node(int(n))
        to_process.append(node)
        if node.data == 0:
            assert zero is None
            zero = node
        if llist.head is None:
            llist.head = node
            head = node
        else:
            node.prev = llist.head
            llist.head.next = node
            llist.head = node

head.prev = llist.head
llist.head.next = head
llist.head = head
length = len(to_process)

# part2
for n in to_process:
    n.data *= 811589153

with timer("part1:"):
    for j in range(10):
        print(j)
        for n in to_process:
            if n.data > 0:
                for i in range(n.data % (length - 1)):
                    n1, n2, n3, n4 = n.prev, n, n.next, n.next.next
                    n1.next = n3
                    n3.prev = n1
                    n3.next = n2
                    n2.prev = n3
                    n2.next = n4
                    n4.prev = n2
            if n.data < 0:
                for i in range(-n.data % (length - 1)):
                    n1, n2, n3, n4 = n.prev.prev, n.prev, n, n.next
                    n1.next = n3
                    n3.prev = n1
                    n3.next = n2
                    n2.prev = n3
                    n2.next = n4
                    n4.prev = n2

    part1 = 0
    node = zero
    for i in range(1000):
        node = node.next
    part1 += node.data
    for i in range(1000):
        node = node.next
    part1 += node.data
    for i in range(1000):
        node = node.next
    part1 += node.data
    print("part1:", part1)


exit()
data = []
with open(input_filename) as fh:
    for l in fh.readlines():
        n = Node(int(l.strip()))
        data.append(n)
        if n.data == 0:
            zero = n

# print([n for _, n in data])
length = len(data)
for n in list(data):  # type: Node
    if n.data == 0:
        # expected.pop(0)
        continue

    start = data.index(n)
    end = (start + n.data)
    end %= length
    if n.data > 0:
        end += 1

    print(f"{n.data=} {start=} {end=}")

    # TODO - don't think this is necessary
    if end == 0 and n.data < 0:
        end = length

    if end < start:
        data[start:start+1] = []
        data[end:end] = [n]
    else:
        data[end:end] = [n]
        data[start:start+1] = []

    # e = expected.pop(0)
    # print([
    #     [n.data for n in data],
    #     e,
    #     [n.data for n in data] == e,
    # ])

# NOT -18746

index = data.index(zero)
print("part1:", sum([
    data[(index + 1000) % len(data)].data,
    data[(index + 2000) % len(data)].data,
    data[(index + 3000) % len(data)].data,
]))

