#!/usr/bin/env python

from pathlib import Path
from rich import print

# https://adventofcode.com/2022/day/7

input_filename = "input7.txt"
input_filename = "sample7.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

cwd = Path('/')

files = {}
dirs = {cwd}

for line in data:
    match line.split(' '):
        case '$', 'cd', x:
            cwd = (cwd / x).resolve()
            dirs.add(cwd)
        case '$', 'ls':
            pass
        case 'dir', _:
            pass
        case size, name:
            size = int(size)
            files[cwd / name] = size

part1 = 0
for dir in dirs:
    total = 0
    for file, size in files.items():
        if str(file).startswith(str(dir)):
            total += size
    if total <= 100000:
        part1 += total

    if dir == Path('/'):
        current_used = total

print("part1:", part1)

max_used = 70000000 - 30000000
need_to_delete = current_used - max_used

part2 = 70000000

for dir in dirs:
    total = 0
    for file, size in files.items():
        if str(file).startswith(str(dir)):
            total += size

    if total >= need_to_delete and total < part2:
        part2 = total

print("part2:", part2)
