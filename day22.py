#!/usr/bin/env python
import re

from aoclib import Vector, print_grid
from rich import print

# https://adventofcode.com/2022/day/22

input_filename = "input22.txt"
input_filename = "sample22.txt"

with open(input_filename) as fh:
    data = [l.strip('\n') for l in fh.readlines()]

grid = {}
for y, line in enumerate(data):
    if not line:
        break
    for x, char in enumerate(line):
        if char != ' ':
            grid[Vector(x, y)] = char

headings = {
    0: Vector(1, 0),
    1: Vector(0, 1),
    2: Vector(-1, 0),
    3: Vector(0, -1),
}

heading_number = 0
position = Vector(min(v.x for v in grid.keys() if v.y == 0), 0)

for instruction in re.findall(r'R|L|\d+', data[-1]):
    match instruction:
        case 'R':
            heading_number = (heading_number + 1) % 4
        case 'L':
            heading_number = heading_number - 1
            if heading_number < 0:
                heading_number = 3
        case f:
            heading = headings[heading_number]
            for _ in range(int(f)):
                # print(['forward', heading])
                new_position = position + heading
                if new_position not in grid:
                    match heading_number:
                        case 0:
                            new_position = Vector(min(v.x for v in grid.keys() if v.y == new_position.y), new_position.y)
                        case 1:
                            new_position = Vector(new_position.x, min(v.y for v in grid.keys() if v.x == new_position.x))
                        case 2:
                            new_position = Vector(max(v.x for v in grid.keys() if v.y == new_position.y), new_position.y)
                        case 3:
                            new_position = Vector(new_position.x, max(v.y for v in grid.keys() if v.x == new_position.x))
                        case '_':
                            raise NotImplementedError()

                if grid[new_position] == '#':
                    break
                position = new_position

print("part1:", (position.y+1)*1000 + (position.x+1) * 4 + heading_number)

print_grid(grid)
