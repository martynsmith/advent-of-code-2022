#!/usr/bin/env python

from rich import print

# https://adventofcode.com/2022/day/11

input_filename = "input11.txt"
input_filename = "sample11.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

monkeys = {
    0: dict(
        items=[77, 69, 76, 77, 50, 58],
        operation=lambda old: old * 11,
        test=lambda v: (v % 5 == 0 and 1 or 5),
    ),
    1: dict(
        items=[75, 70, 82, 83, 96, 64, 62],
        operation=lambda old: old + 8,
        test=lambda v: (v % 17 == 0 and 5 or 6),
    ),
    2: dict(
        items=[53],
        operation=lambda old: old * 3,
        test=lambda v: (0 if v % 2 == 0 else 7),
    ),
    3: dict(
        items=[85, 64, 93, 64, 99],
        operation=lambda old: old + 4,
        test=lambda v: (v % 7 == 0 and 7 or 2),
    ),
    4: dict(
        items=[61, 92, 71],
        operation=lambda old: old * old,
        test=lambda v: (v % 3 == 0 and 2 or 3),
    ),
    5: dict(
        items=[79, 73, 50, 90],
        operation=lambda old: old + 2,
        test=lambda v: (v % 11 == 0 and 4 or 6),
    ),
    6: dict(
        items=[50, 89],
        operation=lambda old: old + 3,
        test=lambda v: (v % 13 == 0 and 4 or 3),
    ),
    7: dict(
        items=[83, 56, 64, 58, 93, 91, 56, 65],
        operation=lambda old: old + 5,
        test=lambda v: (v % 19 == 0 and 1 or 0),
    ),
}

# monkeys = {
#     0: dict(
#         items=[79, 98],
#         operation=lambda old: old * 19,
#         test=lambda v: (v % 23 == 0 and 2 or 3),
#     ),
#     1: dict(
#         items=[54, 65, 75, 74],
#         operation=lambda old: old + 6,
#         test=lambda v: (v % 19 == 0 and 2 or 0),
#     ),
#     2: dict(
#         items=[79, 60, 97],
#         operation=lambda old: old * old,
#         test=lambda v: (v % 13 == 0 and 1 or 3),
#     ),
#     3: dict(
#         items=[74],
#         operation=lambda old: old + 3,
#         test=lambda v: (0 if v % 17 == 0 else 1),
#     ),
# }

for r in range(10000):
    print(r)
    for n in range(len(monkeys)):
        monkey = monkeys[n]
        monkey.setdefault('inspects', 0)
        for item in monkey['items']:
            monkey['inspects'] += 1
            item = monkey['operation'](item)
            # item //= 3
            # item %= 96577
            item %= 9699690
            target = monkey['test'](item)
            monkeys[target]['items'].append(item)
        monkey['items'] = []
    # print(monkeys)

print(sorted([v['inspects'] for v in monkeys.values()]))
print(monkeys)
print("part1:", 239*242)
print("part2:", 122667*122693)
