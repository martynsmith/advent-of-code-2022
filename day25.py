#!/usr/bin/env python

from rich import print

# https://adventofcode.com/2022/day/25

input_filename = "input25.txt"
# input_filename = "sample25.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

lookup = {
    '2': 2,
    '1': 1,
    '0': 0,
    '-': -1,
    '=': -2,
}

part1 = 0
for line in data:
    result = 0
    for p,n in enumerate(reversed(line)):
        result +=5 ** p * lookup[n]

    part1 += result

exp = 0
while part1 / 5 ** exp > 0.5:
    exp += 1
exp -= 1

chars = []
for e in range(exp, -1, -1):
    best_error = 5 ** e
    best_char = None
    for c, v in lookup.items():
        error = abs(5 ** e * v - part1)
        if error < best_error:
            best_error = error
            best_char = c

    chars.append(best_char)
    part1 -= lookup[best_char] * 5 ** e

print("part1:", "".join(chars))