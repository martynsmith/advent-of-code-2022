#!/usr/bin/env python

# https://adventofcode.com/2022/day/1

input_filename = "input1.txt"
# input_filename = "sample1.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

elves = []

current = 0
for line in data:
    if line:
        current += int(line)
    else:
        elves.append(current)
        current = 0

elves.append(current)

print("part1:", max(elves))
print("part2:", sum(sorted(elves)[-3:]))
