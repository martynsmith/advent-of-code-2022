#!/usr/bin/env python

from rich import print
import networkx
import re
import functools

from aoclib import timer

# https://adventofcode.com/2022/day/16

input_filename = "input16.txt"
# input_filename = "sample16.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

g = networkx.Graph()

RATES = {}
PATHS = {}

for line in data:
    match = re.search(r'Valve (\S+) has flow rate=(\d+); tunnels? leads? to valves? (.*)', line)
    valve, rate, leads_to = match.groups()
    if rate != '0':
        RATES[valve] = int(rate)
    for path in leads_to.split(', '):
        g.add_edge(valve, path)

for a in g:
    if a in g:
        for b, l in networkx.single_target_shortest_path_length(g, a):
            if b in RATES:
                PATHS[a, b] = l


@functools.cache
def part1(m=30, v='AA', targets=frozenset(RATES.keys())) -> int:
    rates = [0]
    for t in targets:
        if PATHS[v, t] >= m:
            continue
        rates.append(RATES[t] * (m - PATHS[v, t] - 1) + part1(m - PATHS[v, t] - 1, t, targets - {t}))
    return max(rates)


@functools.cache
def part2(m=26, v='AA', targets=frozenset(RATES.keys()), has_elephant=False) -> int:
    rates = [0]
    for t in targets:
        if PATHS[v, t] >= m:
            continue
        rates.append(
            RATES[t] * (m - PATHS[v, t] - 1) + part2(m - PATHS[v, t] - 1, t, targets - {t}, has_elephant=has_elephant))
    if has_elephant:
        rates.append(part2(26, 'AA', targets))

    return max(rates)


with timer('part1'):
    print("part1:", part1())

with timer('part2'):
    print("part2:", part2(has_elephant=True))
