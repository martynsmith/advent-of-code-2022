#!/usr/bin/env python
from typing import NamedTuple
from rich import print


# https://adventofcode.com/2022/day/9

class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x + p.x, self.y + p.y)


input_filename = "input9.txt"
# input_filename = "sample9.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

directions = dict(
    R=Vector(1, 0),
    U=Vector(0, -1),
    L=Vector(-1, 0),
    D=Vector(0, 1),
)


def move_to(head: Vector, tail: Vector):
    x = tail.x
    y = tail.y
    moved = False
    if x < head.x - 1:
        moved = True
        x += 1
    if x > head.x + 1:
        moved = True
        x -= 1
    if y < head.y - 1:
        moved = True
        y += 1
    if y > head.y + 1:
        moved = True
        y -= 1

    if moved:
        if x == tail.x and x != head.x:
            if x < head.x:
                x += 1
            elif x > head.x:
                x -= 1
        if y == tail.y and y != head.y:
            if y < head.y:
                y += 1
            elif y > head.y:
                y -= 1

    return Vector(x, y)


head = Vector(0, 0)

head_path = [head]

for line in data:
    direction, length = line.split(' ')
    length = int(length)
    for i in range(length):
        head += directions[direction]
        head_path.append(head)


def tail_path(head_path: list[Vector]) -> list[Vector]:
    tail_path: list[Vector] = []
    tail = head_path[0]
    for head in head_path[1:]:
        tail = move_to(head, tail)
        tail_path.append(tail)

    return tail_path


print("part1:", len(set(tail_path(head_path))))

path = head_path
for i in range(9):
    path = tail_path(path)

print("part2:", len(set(path)))
