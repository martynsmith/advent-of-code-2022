#!/usr/bin/env python
import functools

from rich import print

from aoclib import Vector3, get_3d_bounds, timer

# https://adventofcode.com/2022/day/18

input_filename = "input18.txt"
# input_filename = "sample18.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

lava = set()

for line in data:
    p = Vector3(*map(int, line.split(',')))
    lava.add(p)

part1 = 0
for l in lava:
    for n in l.get_neighbours():
        if n not in lava:
            part1 += 1

print("part1:", part1)

# -----

part2 = 0
bounds = get_3d_bounds(lava)

good = set()
bad = set()


@functools.cache
def is_contained(v: Vector3):
    seen = {v}
    to_check = {v}
    last_seen_size = 0

    while len(seen) != last_seen_size:
        last_seen_size = len(seen)
        for v in list(to_check):
            to_check.discard(v)
            for n in v.get_neighbours():
                if n.x < bounds[0] or n.x > bounds[1] or n.y < bounds[2] or n.y > bounds[3] or n.z < bounds[4] or n.z > \
                        bounds[5]:
                    # Out of bounds
                    bad.update(seen)
                    return False
                if n in lava:
                    # This is a wall, stop
                    continue
                if n in seen:
                    # Already been here
                    continue
                seen.add(n)
                to_check.add(n)

    good.update(seen)
    return True


for l in lava:
    for n in l.get_neighbours():
        if n in lava:
            continue
        if n in bad:
            continue
        elif n in good or is_contained(n):
            part2 += 1

with timer("part2:"):
    print("part2:", part1 - part2)
