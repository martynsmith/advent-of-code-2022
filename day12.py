#!/usr/bin/env python

from typing import NamedTuple

import networkx
from rich import print
from aocd.models import Puzzle

puzzle = Puzzle(year=2022, day=12)

# https://adventofcode.com/2022/day/12

data = puzzle.input_data
# data = puzzle.example_data
data = [l.strip() for l in data.splitlines()]


class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x + p.x, self.y + p.y)


directions = [
    Vector(1, 0),
    Vector(-1, 0),
    Vector(0, 1),
    Vector(0, -1),
]


g = networkx.DiGraph()

# Parse the points
for y, line in enumerate(data):
    for x, char in enumerate(line):
        if char == 'S':
            start = Vector(x, y)
            char = 'a'
        if char == 'E':
            end = Vector(x, y)
            char = 'z'

        g.add_node(Vector(x, y), height=ord(char) - ord('a'))

# Add the edges
for p in g:
    for d in directions:
        n = p + d
        if n in g.nodes and g.nodes[n]['height'] <= g.nodes[p]['height'] + 1:
            g.add_edge(p, n)

# Shortest path from start to end
puzzle.answer_a = networkx.shortest_path_length(g, start, end)
print("part1:", puzzle.answer_a)

puzzle.answer_b = min(
    l
    for p, l in networkx.single_target_shortest_path_length(g, end)
    if g.nodes[p]['height'] == 0
)
print("part2:", puzzle.answer_b)
