#!/usr/bin/env python

from typing import NamedTuple
# from rich import print

# https://adventofcode.com/2022/day/14

input_filename = "input14.txt"
# input_filename = "sample14.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]


class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x + p.x, self.y + p.y)


def print_grid(cells):
    mx = max(v.x for v in cells.keys())
    my = max(v.y for v in cells.keys())

    for y in range(my + 1):
        for x in range(490, mx + 1):
            print(cells.get(Vector(x, y), '.'), end="")
        print()


grid = {}

for line in data:
    points = [Vector(*map(int, l.split(','))) for l in line.split(' -> ')]
    s = points.pop(0)
    for p in points:
        grid[s] = '#'
        if s.x == p.x:
            d = 1 if s.y < p.y else -1
            while s != p:
                s = s + Vector(0, d)
                grid[s] = '#'
        else:
            d = 1 if s.x < p.x else -1
            while s != p:
                s = s + Vector(d, 0)
                grid[s] = '#'


movements = [
    Vector(0, 1),
    Vector(-1, 1),
    Vector(1, 1),
]

max_y = max(v.y for v in grid.keys())
part1 = 0
full = False

while not full:
    s = Vector(500, 0)
    while not full:
        for m in movements:
            if grid.get(s + m) not in ['#', 'O']:
                break

        if grid.get(s + m) not in ['#', 'O']:
            s += m
            if s.y > max_y + 1:
                full = True
            continue

        if not full:
            grid[s] = 'O'
            part1 += 1
        break

print("part1:", part1)

min_x = min(v.x for v in grid.keys())
max_x = max(v.x for v in grid.keys())

for x in range(min_x - 5000, max_x + 5000):
    grid[Vector(x, max_y+2)] = '#'

full = False
while not full:
    s = Vector(500, 0)
    while not full:
        for m in movements:
            if grid.get(s + m) not in ['#', 'O']:
                break

        if grid.get(s + m) not in ['#', 'O']:
            s += m
            if s == Vector(500, 0):
                full = True
            continue

        if not full:
            grid[s] = 'O'
            part1 += 1

        if s == Vector(500, 0):
            full = True
        break

print("part2:", part1)
