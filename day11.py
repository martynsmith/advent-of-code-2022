#!/usr/bin/env python

from typing import Callable
from rich import print
from dataclasses import dataclass
from math import prod

# https://adventofcode.com/2022/day/11

input_filename = "input11.txt"
# input_filename = "sample11.txt"


@dataclass
class Monkey:
    items: list[int]
    inspections: int = 0
    divisor: int = 0
    true_target: int = 0
    false_target: int = 0
    operation: Callable = lambda n: n


with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]


# PARSING
def get_monkeys():
    monkeys = {}

    current_monkey = None

    for line in data:
        monkey = monkeys.get(current_monkey, None)
        match line.strip().split(' '):
            case 'Monkey', n:
                current_monkey = int(n[:-1])
                monkeys[current_monkey] = Monkey(
                    items=[],
                )
            case 'Starting', 'items:', *items:
                monkey.items = [int(v.replace(',', '')) for v in items]
            case 'Operation:', 'new', '=', *func:
                monkey.operation = eval("lambda old: " + "".join(func))
            case 'Test:', 'divisible', 'by', divisor:
                monkey.divisor = int(divisor)
            case 'If', result, 'throw', 'to', 'monkey', target:
                if result == 'true:':
                    monkey.true_target = int(target)
                else:
                    monkey.false_target = int(target)
            case ['']:
                pass
            case _:
                raise NotImplementedError(line)

    return monkeys


# PART 1

monkeys = get_monkeys()

for r in range(20):
    for n in range(len(monkeys)):
        monkey = monkeys[n]
        for item in monkey.items:
            monkey.inspections += 1
            item = monkey.operation(item)
            item //= 3
            if item % monkey.divisor == 0:
                target = monkey.true_target
            else:
                target = monkey.false_target

            monkeys[target].items.append(item)
        monkey.items = []

print(prod(sorted([m.inspections for m in monkeys.values()])[-2:]))

# PART 2

monkeys = get_monkeys()
divisor_prod = prod(m.divisor for m in monkeys.values())

for r in range(10000):
    for n in range(len(monkeys)):
        monkey = monkeys[n]
        for item in monkey.items:
            monkey.inspections += 1
            item = monkey.operation(item)
            item %= divisor_prod
            if item % monkey.divisor == 0:
                target = monkey.true_target
            else:
                target = monkey.false_target

            monkeys[target].items.append(item)
        monkey.items = []

print(prod(sorted([m.inspections for m in monkeys.values()])[-2:]))
