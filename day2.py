#!/usr/bin/env python

# https://adventofcode.com/2022/day/2

input_filename = "input2.txt"
# input_filename = "sample2.txt"

move_map = dict(
    A='R',
    B='P',
    C='S',
    X='R',
    Y='P',
    Z='S',
)
beats = dict(R='S', P='R', S='P')
loses = {v: k for k, v in beats.items()}
shape_score = dict(R=1, P=2, S=3)

with open(input_filename) as fh:
    data = [tuple(move_map[c] for c in l.strip().split(' ')) for l in fh.readlines()]

part1 = 0
for opp, me in data:
    part1 += shape_score[me]
    if opp == me:
        part1 += 3
    elif beats[me] == opp:
        part1 += 6

print("part1:", part1)

part2 = 0
for opp, outcome in data:
    match outcome:
        case 'R':
            me = beats[opp]
        case 'P':
            part2 += 3
            me = opp
        case 'S':
            part2 += 6
            me = loses[opp]

    part2 += shape_score[me]

print("part2:", part2)
