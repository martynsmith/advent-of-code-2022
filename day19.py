#!/usr/bin/env python

from __future__ import annotations

import functools
from typing import NamedTuple
from dataclasses import dataclass

from rich import print
import re

from aoclib import timer

# https://adventofcode.com/2022/day/19

input_filename = "input19.txt"
# input_filename = "sample19.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]


class Source(NamedTuple):
    n: int
    type: str


class Resources(NamedTuple):
    ore: int
    clay: int
    obsidian: int
    geode: int

    def __repr__(self):
        if self == ORE:
            return '<Ore>'
        if self == CLAY:
            return '<Clay>'
        if self == OBSIDIAN:
            return '<Obsidian>'
        if self == GEODE:
            return '<Geode>'

        return f"<Resource ore={self.ore} clay={self.clay} obsidian={self.obsidian} geode={self.geode}>"
        #     return super().__repr__()

    def __add__(self, other: Resources) -> Resources:
        return Resources(
            self.ore + other.ore,
            self.clay + other.clay,
            self.obsidian + other.obsidian,
            self.geode + other.geode,
        )

    def __sub__(self, other: Resources) -> Resources:
        return Resources(
            self.ore - other.ore,
            self.clay - other.clay,
            self.obsidian - other.obsidian,
            self.geode - other.geode,
        )

    def __mul__(self, other: int) -> Resources:
        return Resources(
            self.ore * other,
            self.clay * other,
            self.obsidian * other,
            self.geode * other,
        )

    def fits_in(self, other):
        return self.ore <= other.ore and self.clay <= other.clay and self.obsidian <= other.obsidian and self.geode <= other.geode

    def __lt__(self, other: Resources) -> bool:
        raise NotImplementedError()
        return self.ore < other.ore and self.clay < other.clay and self.obsidian < other.obsidian and self.geode < other.geode

    def __le__(self, other: Resources) -> bool:
        raise NotImplementedError()
        return self.ore <= other.ore and self.clay <= other.clay and self.obsidian <= other.obsidian and self.geode <= other.geode

    def __gt__(self, other: Resources) -> bool:
        raise NotImplementedError()
        return self.ore > other.ore and self.clay > other.clay and self.obsidian > other.obsidian and self.geode > other.geode

    def __ge__(self, other: Resources) -> bool:
        raise NotImplementedError()
        return self.ore >= other.ore and self.clay >= other.clay and self.obsidian >= other.obsidian and self.geode >= other.geode


ORE = Resources(1, 0, 0, 0)
CLAY = Resources(0, 1, 0, 0)
OBSIDIAN = Resources(0, 0, 1, 0)
GEODE = Resources(0, 0, 0, 1)


class Blueprint(NamedTuple):
    n: int
    ore: Resources
    clay: Resources
    obsidian: Resources
    geode: Resources

    @property
    @functools.cache
    def max_ore_cost(self):
        return max(self.ore.ore, self.clay.ore, self.obsidian.ore, self.geode.ore)

    @property
    @functools.cache
    def max_clay_cost(self):
        return max(self.ore.clay, self.clay.clay, self.obsidian.clay, self.geode.clay)


blueprints: list[Blueprint] = []

ore = clay = obsidian = geode = Resources(0, 0, 0, 0)
for line in data:
    if m := re.match('Blueprint (\d+)', line):
        if ore.ore or ore.clay or ore.obsidian or ore.geode:
            blueprints.append(Blueprint(
                n=int(m.group(1)) - 1,
                ore=ore,
                clay=clay,
                obsidian=obsidian,
                geode=geode,
            ))
        ore = clay = obsidian = geode = Resources(0, 0, 0, 0)
        continue
    if m := re.match('Each (\S+) robot costs? (.*)', line):
        target = m.group(1)
        for n, source in [g.split(' ') for g in m.group(2).strip('.').split(' and ')]:
            match source:
                case 'ore':
                    r = Resources(int(n), 0, 0, 0)
                case 'clay':
                    r = Resources(0, int(n), 0, 0)
                case 'obsidian':
                    r = Resources(0, 0, int(n), 0)
                case 'geode':
                    r = Resources(0, 0, 0, int(n))

            match target:
                case 'ore':
                    ore += r
                case 'clay':
                    clay += r
                case 'obsidian':
                    obsidian += r
                case 'geode':
                    geode += r
                case _:
                    raise NotImplementedError(target)
        continue

    if not line:
        continue

    raise NotImplementedError(line)

blueprints.append(Blueprint(
    n=blueprints[-1].n+1,
    ore=ore,
    clay=clay,
    obsidian=obsidian,
    geode=geode,
))


def find_potential(blueprint: Blueprint, resources: Resources):
    potential = set()

    if blueprint.geode.fits_in(resources):
        potential.add((Resources(0, 0, 0, 1), blueprint.geode))
    if blueprint.obsidian.fits_in(resources):
        potential.add((Resources(0, 0, 1, 0), blueprint.obsidian))
    if blueprint.clay.fits_in(resources):
        potential.add((Resources(0, 1, 0, 0), blueprint.clay))
    # if blueprint.ore.fits_in(resources):
    #     potential.add((Resources(1, 0, 0, 0), blueprint.ore))

    return potential


@functools.cache
def part1(blueprint: Blueprint, minutes=24, robots=Resources(1, 0, 0, 0), resources=Resources(0, 0, 0, 0), path=None):
    # print('---')
    if path is None:
        path = ()

    if minutes <= 0:
        return (resources, path)

    projection = resources + robots * minutes
    # print(['project:', minutes, robots, projection])
    need = {
        GEODE: 1,
        OBSIDIAN: 0,
        CLAY: 0,
        ORE: 0,
    }

    if not (blueprint.geode * need[GEODE]).fits_in(projection):
        calc = projection - blueprint.geode * need[GEODE]
        if calc.ore < 0: need[ORE] -= calc.ore
        if calc.clay < 0: need[CLAY] -= calc.clay
        if calc.obsidian < 0: need[OBSIDIAN] -= calc.obsidian
    if not (blueprint.geode * need[OBSIDIAN]).fits_in(projection):
        calc = projection - blueprint.obsidian * need[OBSIDIAN]
        if calc.ore < 0: need[ORE] -= calc.ore
        if calc.clay < 0: need[CLAY] -= calc.clay
    if not (blueprint.clay * need[CLAY]).fits_in(projection):
        calc = projection - blueprint.clay * need[CLAY]
        if calc.ore < 0: need[ORE] -= calc.ore

    # print(['need:', need])

    paths = []

    potential = find_potential(blueprint, resources)

    for robot, cost in potential:
        if need[robot] <= 0:
            continue
        # print(['appending', minutes, robot])
        paths.append(
            # part1(blueprint, minutes - 1, robots=robots + robot, resources=resources + robots - cost, path=tuple([*path, f"{25-minutes} buying {robot} => {resources+robots=}"]))
            part1(blueprint, minutes - 1, robots=robots + robot, resources=resources + robots - cost, path=path)
        )

    paths.append(
        # part1(blueprint, minutes - 1, robots, resources + robots, path=path)
        part1(
            blueprint,
            minutes - 1,
            robots,
            resources + robots,
            path=tuple([*path, f"{need=} {projection=}"])
        ),
    )

    return max(paths, key=lambda r: r[0].geode)


def part1a(blueprint: Blueprint, minutes=24):
    robots = Resources(1, 0, 0, 0)
    resources = Resources(0, 0, 0, 0)

    while minutes > 0:
        resources += robots
        print(f"{25-minutes} {resources=} {robots=}")

        if blueprint.geode.fits_in(resources):
            print("buy geode")
            resources -= blueprint.geode
            robots += GEODE
            resources -= GEODE
        elif blueprint.geode.fits_in(resources + robots * 2):
            pass
        elif blueprint.obsidian.fits_in(resources):
            print("buy obsidian")
            resources -= blueprint.obsidian
            robots += OBSIDIAN
            resources -= OBSIDIAN
        elif blueprint.obsidian.fits_in(resources + robots * 2):
            pass
        elif blueprint.clay.fits_in(resources):
            print("buy clay")
            resources -= blueprint.clay
            robots += CLAY
            resources -= CLAY

        minutes -= 1

    return resources


def part1b(blueprint: Blueprint, minutes=24, robots=Resources(1, 0, 0, 0), resources=Resources(0, 0, 0, 0), path=None):
    if path is None:
        path = dict(max=0)
    if minutes <= 0:
        if resources.geode > path['max']:
            path['max'] = resources.geode
        return resources, path

    # No chance to exceed more geodes!
    projection = resources + robots * minutes
    if (projection + Resources(0, 0, 0, sum(range(minutes)))).geode <= path['max']:
        return resources, path

    options = []
    for robot, cost in [
        (ORE, blueprint.ore),
        (CLAY, blueprint.clay),
        (OBSIDIAN, blueprint.obsidian),
        (GEODE, blueprint.geode),
    ]:
        if robot == CLAY and robots.clay >= blueprint.max_clay_cost:
            continue
        if robot == OBSIDIAN and projection.obsidian + minutes < blueprint.geode.obsidian:
            continue

        # Yeah, some random hard-coded pruning constants ... wanna fight about it?
        if robot == ORE and minutes < 0:
            continue
        if robot == CLAY and minutes < 0:
            continue

        rb = robots
        r = resources + rb
        m = minutes - 1
        while not cost.fits_in(r) and m > 0:
            m -= 1
            r += rb

        if m != 0:
            rb += robot
            r -= cost
            options.append(part1b(blueprint, m, rb, r - robot, path=path))

    if not options:
        return part1b(blueprint, 0, robots, resources + robots * minutes, path=path)

    return max(options, key=lambda r: r[0].geode)


part1 = 0
with timer('part1:'):
    for b in blueprints:
        r = part1b(b)[0].geode
        print(r)
        part1 += b.n * r

print('part1:', part1)

exit()

part2 = 1
blueprints = blueprints[:3]
with timer('part2:'):
    for b in blueprints:
        r = part1b(b, 32)[0].geode
        print(r)
        part2 *= r
print('part2:', part2)
