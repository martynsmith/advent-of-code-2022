from __future__ import annotations
from contextlib import contextmanager
import time
from typing import NamedTuple, Iterable


@contextmanager
def timer(description):
    start = time.perf_counter_ns()
    yield
    end = time.perf_counter_ns()
    ms = (end - start) / 10 ** 6
    print(f"{description}: {ms}ms")


class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x + p.x, self.y + p.y)


def print_grid(cells):
    minx = min(v.x for v in cells.keys())
    miny = min(v.y for v in cells.keys())
    maxx = max(v.x for v in cells.keys())
    maxy = max(v.y for v in cells.keys())

    for y in range(miny, maxy + 1):
        for x in range(minx, maxx + 1):
            print(cells.get(Vector(x, y), '.'), end="")
        print()


class Vector3(NamedTuple):
    x: int
    y: int
    z: int

    def __add__(self, p) -> Vector3:
        return Vector3(self.x + p.x, self.y + p.y, self.z + p.z)

    def get_neighbours(self):
        return [self + v for v in NEIGHBOURS_3]


NEIGHBOURS_3 = (
    Vector3(0, 0, -1),
    Vector3(0, 0, 1),
    Vector3(0, -1, 0),
    Vector3(0, 1, 0),
    Vector3(-1, 0, 0),
    Vector3(1, 0, 0),
)


def get_3d_bounds(vectors: Iterable[Vector3]):
    v1 = list(vectors)[0]

    bounds = [v1.x, v1.x, v1.y, v1.y, v1.z, v1.z]

    for v in vectors:
        bounds[0] = min(bounds[0], v.x)
        bounds[1] = max(bounds[1], v.x)
        bounds[2] = min(bounds[2], v.y)
        bounds[3] = max(bounds[3], v.y)
        bounds[4] = min(bounds[4], v.z)
        bounds[5] = max(bounds[5], v.z)

    return bounds
