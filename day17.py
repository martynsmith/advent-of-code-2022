#!/usr/bin/env python
import functools

from rich import print
from itertools import cycle

from aoclib import Vector, print_grid, timer

# https://adventofcode.com/2022/day/17

input_filename = "input17.txt"
# input_filename = "sample17.txt"

rocks_source = [
    (
        Vector(2, 0),
        Vector(3, 0),
        Vector(4, 0),
        Vector(5, 0),
    ),
    (
        Vector(3, -2),
        Vector(2, -1),
        Vector(3, -1),
        Vector(4, -1),
        Vector(3, 0),
    ),
    (
        Vector(4, -2),
        Vector(4, -1),
        Vector(2, 0),
        Vector(3, 0),
        Vector(4, 0),
    ),
    (
        Vector(2, -3),
        Vector(2, -2),
        Vector(2, -1),
        Vector(2, 0),
    ),
    (
        Vector(2, -1),
        Vector(3, -1),
        Vector(2, 0),
        Vector(3, 0),
    ),
]
pushes_source = []

with open(input_filename) as fh:
    data = fh.read().strip()

for c in data:
    match c:
        case '<':
            pushes_source.append(Vector(-1, 0))
        case '>':
            pushes_source.append(Vector(1, 0))
        case _:
            raise NotImplementedError(f"{c=}")

rock_count = 5
push_count = len(pushes_source)


def display(rock=None):
    base = {
        Vector(-1, 0): '+',
        Vector(7, 0): '+',
    }
    for x in range(7):
        base[Vector(x, 0)] = '-'

    if rock:
        for v in rock:
            base[v] = '@'

    for y in range(min((v.y for v in base.keys()), default=0) - 1, 0):
        base[Vector(-1, y)] = '|'
        base[Vector(7, y)] = '|'

    base.update({v: '#' for v in fallen})
    print_grid(base)
    print()


def part1(rock_target: int, get_top=False):
    if rock_target == 0:
        return 0
    rock_count = 0
    fallen = set()
    min_y = 0
    # print(len(rocks_source), len(pushes_source), rock_target)
    rocks = cycle(rocks_source)
    pushes = cycle(pushes_source)
    while True:
        rock = next(rocks)
        rock_count += 1
        rock = [v + Vector(0, -4 + min_y) for v in rock]
        # display(rock)
        while True:
            push = next(pushes)
            test_rock = [v + push for v in rock]

            if not any(v in fallen or v.x < 0 or v.x > 6 for v in test_rock):
                rock = test_rock

            # display(rock)
            test_rock = [v + Vector(0, 1) for v in rock]
            if any(v in fallen or v.y >= 0 for v in test_rock):
                fallen.update(rock)
                min_y = min([min_y] + [v.y for v in rock])
                break

            rock = test_rock

        if rock_count % 50455 == 0:
            tmp = tuple(min(v.y - min_y for v in fallen if v.x == x) for x in range(7))
            print([
                rock_count // 50455, min_y, tmp,
                tmp == (5, 5, 5, 0, 4, 4, 2),
                tmp == (12, 4, 4, 6, 0, 7, 7),
            ])
        if rock_count == rock_target:
            break

    # display()
    if get_top:
        return -min_y, [min(v.y - min_y for v in fallen if v.x == x) for x in range(7)]
    return -min_y


with timer('part1:'):
    print("part1:", part1(2022))

with timer('part2:'):
    base_cycle_rocks = push_count * rock_count
    base_cycle_height = part1(base_cycle_rocks)

    # print(f"{base_cycle_rocks=}")
    part1(base_cycle_rocks * 1000)

    print(f"{base_cycle_rocks=}")
    print(f"{base_cycle_height=}")

    cycle_rocks = base_cycle_rocks * 7
    cycle_height = 2120

    cycle_count = (1000000000000 - base_cycle_rocks) // cycle_rocks
    remainder_rocks = (1000000000000 - base_cycle_rocks) % (cycle_count * cycle_rocks)

    print(f"{remainder_rocks=}")
    print("low:  ", 1577128133946)
    print("sample", 1514285714288)
    remainder_height = part1(base_cycle_rocks + remainder_rocks) - base_cycle_height
    print(f"{remainder_height=}")
    part2 = part1(push_count * rock_count) + cycle_count * cycle_height + remainder_height
    print("part2:", 1514285714288 - part2)
    print("part2:", part2)
