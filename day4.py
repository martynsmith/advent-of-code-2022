#!/usr/bin/env python

import re

# https://adventofcode.com/2022/day/4

input_filename = "input4.txt"
# input_filename = "sample4.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

part1 = 0
part2 = 0

for line in data:
    a, b, c, d = map(int, re.findall('(\d+)', line))
    e1 = set(range(a, b+1))
    e2 = set(range(c, d+1))

    if e1.issubset(e2) or e2.issubset(e1):
        part1 += 1
    if e1 & e2:
        part2 += 1

print("part1:", part1)
print("part2:", part2)
