#!/usr/bin/env python

# https://adventofcode.com/2022/day/6

input_filename = "input6.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()][0]

for i in range(len(data)):
    chars = set(data[i:i+4])
    if len(chars) < 4:
        continue
    print("part1:", i+4)
    break

for i in range(len(data)):
    chars = set(data[i:i+14])
    if len(chars) < 14:
        continue
    print("part2:", i+14)
    break
