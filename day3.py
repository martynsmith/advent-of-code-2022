#!/usr/bin/env python

# https://adventofcode.com/2022/day/3

input_filename = "input3.txt"
# input_filename = "sample3.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

def get_priority(i):
    if i.islower():
        return ord(i) - ord('a') + 1
    else:
        return ord(i) - ord('A') + 27

part1 = 0
for line in data:
    a = set(line[:len(line)//2])
    b = set(line[len(line)//2:])
    i:str = list(a.intersection(b))[0]
    part1 += get_priority(i)

print("part1:", part1)

wip = None
part2 = 0
for i, line in enumerate(data):
    if i % 3 == 0:
        if wip:
            part2 += get_priority(list(wip)[0])
        wip = set(line)
    wip = wip.intersection(set(line))

part2 += get_priority(list(wip)[0])
print("part2:", part2)
