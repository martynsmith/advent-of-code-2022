#!/usr/bin/env python
from typing import NamedTuple


# https://adventofcode.com/2022/day/8

class Vector(NamedTuple):
    x: int
    y: int

    def __add__(self, p) -> 'Vector':
        return Vector(self.x+p.x, self.y+p.y)

input_filename = "input8.txt"
# input_filename = "sample8.txt"

with open(input_filename) as fh:
    data = [l.strip() for l in fh.readlines()]

points = {}

for y, line in enumerate(data):
    for x, height in enumerate(line):
        points[Vector(x, y)] = height

max_x = max(x for x in points.keys())
max_y = max(y for y in points.keys())

directions = [
    Vector(1, 0),
    Vector(-1, 0),
    Vector(0, 1),
    Vector(0, -1),
]

part1 = 0

for point, height in points.items():
    for direction in directions:
        can_see = True
        p = point
        while p + direction in points:
            p = p + direction
            if points[p] >= height:
                can_see = False
                break
        if can_see:
            part1 += 1
            break

print("part1:", part1)

part2 = 0
for point, height in points.items():
    # point = Vector(2, 1)
    viewing = 1
    for direction in directions:
        distance = 0
        p = point
        while p + direction in points:
            distance += 1
            p = p + direction
            if points[p] >= height:
                break
        viewing *= distance
    part2 = max(part2, viewing)

print("part2:", part2)
